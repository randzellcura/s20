console.log('Hello World!');

for (let count1 = prompt("Enter a number: "); count1 > 50; count1--) {

	if (count1 % 10 === 0) {
		console.log("This number is divisible by 10. Skipping the number.")
		continue;
	}
	if (count1 % 5) {
		continue;
	}
	if (count1 === 50) {
		
		break;
	}
	
	console.log(count1)
	
}
console.log("The current value is at 50. Terminating the loop.")


function remVowel(str) {
	let al = ['a', 'e', 'i', 'o', 'u',
		'A', 'E', 'I', 'O', 'U'];
	let result = "";

	for (let i = 0; i < str.length; i++) {

		if (!al.includes(str[i])) {
			result += str[i];
		}
	}
	return result;
}

// Driver code
let str = "supercalifragilisticexpialidocious";
console.log(str)
console.log(remVowel(str));


